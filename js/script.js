"use strict";

window.addEventListener('load', task);

function task(){
//   ( function MyApp(){
//   // console.log('init function');
//   // https://www.json-generator.com/#
//   var fetchedData = fetch('http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2').then(function(response) {
//     return response.json();
//   }).then(data => {
//     console.log(renderInterface(data));
//   });

// }());
  MyApp();

  var btn_new_link = document.getElementById('useNewLink');
  btn_new_link.addEventListener('click', changeLink);

  function changeLink(){
    var inputData = document.getElementById('newLink');
    MyApp( inputData.value);
    input.value = '';    
  };

  function MyApp(link){
     link = link || 'http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2';
                      
    // console.log('init function');
    // https://www.json-generator.com/#
    var fetchedData = fetch(link).then(function(response) {
      if(response.status!==200){
          alert('Хьюстон, ви хэв э проблем');
      };
      return response.json();
    }).then(data => {
      
      var result = renderInterface(data);

      console.log(result);
      console.log(data);
     var div = document.getElementById('result');
      div.innerHTML = '';
      result.forEach( (item, i) =>

        //var t;
        {var h3Text = 'List ' + (i + 1);
          createUl(item, div, h3Text);}
        );
    });

  };


  function renderInterface( data ){
     //console.log('do some stuff with data:', data);
     
     var firstList = data.filter( (item) => 
      { return item.favoriteFruit.toLowerCase() === 'banana';}
     );

     var secondList = data.filter( (item) =>
        {
         item.balance = Number(item.balance.replace(/[\,\$]/g, ''));
         return item.age > 25 && item.balance > 2000 
       }
      );

     var thirdList = data.filter( (item) =>
        {return item.eyeColor.toLowerCase() === 'blue' &&
                item.gender.toLowerCase() === 'female' &&
                item.isActive === false;
        }
      );

      // var result = {
      //   first: firstList,
      //   second: secondList,
      //   third: thirdList
      // };
      var result = [firstList, secondList, thirdList];
     return result;
  };

  function createUl(obj, div, h3Text){
    var ul = document.createElement('ul');

    for( var i = 0; i < obj.length; i++ ){
      console.log(obj[i]);
      var li = document.createElement('li');
      li.innerHTML = obj[i].name;
      ul.appendChild(li);
    }

    var h3 = document.createElement('h3');
    h3.innerHTML = h3Text;

    div.appendChild(h3);
    div.appendChild(ul);

  };
       
};